import sys
import time
import logging
from watchdog.observers import Observer
from sqlitehandler import SQLiteHandler
from argparse import ArgumentParser

def parse():
    parser = ArgumentParser()

    parser.add_argument("-x", "--extensions", dest="extensions",
                        help="list of file extensions to monitor")
    parser.add_argument("-e", "--events", dest="events", 
                        help="list of events to monitor")
    parser.add_argument("-d", "--directory", dest="directory", 
                        help="directory to monitor")

    return parser.parse_args()

if __name__ == "__main__":
    # logging.basicConfig(level=logging.INFO,
    #                     format='%(asctime)s - %(message)s',
    #                     datefmt='%Y-%m-%d %H:%M:%S')
    args = parse()
    path = '.'
    if args.directory:
        path = args.directory

    # looking for all extensions by default unless a list of extensions are given as argument

    if args.extensions: 
        patterns = ['*.'+x for x in args.extensions.split(',')]
        print('patrolling for {} files in {}'.format(patterns, path))
        handler = SQLiteHandler(patterns=patterns, ignore_directories=True,)
    else:
        print('patrolling for all files in {}'.format(patterns, path))
        handler = SQLiteHandler(ignore_directories=True,)

    # pass db, table names as arugment if not using the default names
    # a table will be created if not exists already in the database
    handler.configure()
    # if events are specified using arugments, set patrolling only for those events
    # allowed events  are - CREATE, DELETE, MODIFY, MOVE
    # if none of these are mentioned than look for all events
    events = []
    if args.events: 
        events = [x for x in args.events.split(',')]
        print(events)
    if len(events) > 0:
        handler.set_events(events)

    observer = Observer()
    observer.schedule(handler, path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()