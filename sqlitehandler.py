import time
import sqlite3 as sq
from watchdog.events import FileSystemEventHandler
from watchdog.events import PatternMatchingEventHandler

class SQLiteHandler(PatternMatchingEventHandler):
    
    # db = 'filepatroldb'
    # table = 'events'

    def configure(self, db='filepatroldb', table='events'):
        self.db = db
        self.table = table

        # prepare sql statements with parameters
        sql = 'CREATE TABLE IF NOT EXISTS events (id INTEGER PRIMARY KEY AUTOINCREMENT, filename TEXT, flag INTEGER DEFAULT 0, timestamp DATETIME, event TEXT, created_at DATETIME DEFAULT CURRENT_TIMESTAMP, updated_at DATETIME, deleted_at DATETIME)'

        # perform create table operation
        # get the connection
        conn = self.connect()
        try:
            cur = conn.cursor()
            cur.execute(sql)
        except sq.Error as sqe:
            print(sqe)
        except Exception as e:
            print(e)
        finally:
            # persist the changes in dbs. commit and close are important!
            conn.commit()
            conn.close()
        

    def set_events(self, events):
        self.events = events
        if ('CREATE' not in events) and ('DELETE' not in events) and ('MODIFY' not in events) and ('MOVE' not in events):
            self.events = ['CREATE', 'DELETE', 'MODIFY', 'MOVE']

    def connect(self):
        conn = None
        try:
            conn = sq.connect(self.db)
            # try creating the table if not already existings
        except sq.Error as sqe:
            print(sqe)
        except Exception as ex:
            print(ex)
    
        return conn

    def save(self, event, event_type):
        # create row tuple for table
        filename = event.src_path
        timestamp = time.time()
        flag = 0
        event = event_type
        
        # prepare sql statements with parameters
        sql = 'INSERT INTO ' + self.table + '(filename,timestamp,flag,event) VALUES(?,?,?,?)'
        row = (filename, timestamp, flag, event)

        # perform database operation
        # get the connection
        conn = self.connect()
        last_row_id = -1
        try:
            cur = conn.cursor()
            cur.execute(sql, row)
            last_row_id = cur.lastrowid
        except sq.Error as sqe:
            print(sqe)
        except Exception as e:
            print(e)
        finally:
            # persist the changes in dbs. commit and close are important!
            conn.commit()
            conn.close()

        return last_row_id

    def on_created(self, event):
        if 'CREATE' in self.events:
            return self.save(event, 'CREATE')

    def on_deleted(self, event):
        if 'DELETE' in self.events:
            return self.save(event, 'DELETE')

    def on_modified(self, event):
        if 'MODIFY' in self.events:
            return self.save(event, 'MODIFY')

    def on_moved(self, event):
        if 'MOVE' in self.events:
            return self.save(event, 'MOVE')

